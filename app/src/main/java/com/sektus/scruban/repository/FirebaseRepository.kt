package com.sektus.scruban.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.Timestamp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.sektus.scruban.data.model.Project

object FirebaseRepository {
    private val db = FirebaseFirestore.getInstance()
    private val auth = FirebaseAuth.getInstance()
    private var user = auth.currentUser
    private val project = MutableLiveData<Project?>(null)

    fun getProject(): LiveData<Project> {
        return project as LiveData<Project>
    }

    fun setProject(p: Project) {
        project.value = p
        db.collection("projects").document(p.id).addSnapshotListener { snapshot, error ->
            project.value = documentToProject(snapshot)
        }
    }

    private fun documentToProject(item: DocumentSnapshot?): Project? {
        return if (item != null) {
            Project(
                item.get("title") as String,
                item.getTimestamp("updateTime") as Timestamp,
                item.id
            )
        } else {
            null
        }
    }

    fun closeProject() {
        project.value = null
    }

    fun getTasksCollection(): CollectionReference {
        return db.collection("users").document(user!!.uid).collection("tasks")
    }

    fun getNotesCollection(): CollectionReference {
        return db.collection("users").document(user!!.uid).collection("notes")
    }

    fun getUserProjectsCollection(): CollectionReference {
        return db.collection("users").document(user!!.uid).collection("projects")
    }

    fun getProjectsCollection(): CollectionReference {
        return db.collection("projects")
    }

    fun getEventsCollection(): CollectionReference {
        return db.collection("users").document(user!!.uid).collection("events")
    }


    fun getUsersCollection(): CollectionReference {
        return db.collection("users")
    }

    fun getProjectTasksCollection(): CollectionReference {
        return db.collection("projects").document(project.value!!.id).collection("tasks")
    }

    fun getProjectHistoriesCollection(): CollectionReference {
        return db.collection("projects").document(project.value!!.id).collection("history")
    }

    fun getProjectDocument(p: Project): DocumentReference {
        return db.collection("projects").document(p.id)
    }

    fun getUserDocument(): DocumentReference {
        return db.collection("users").document(user!!.uid)
    }

    fun getRoot(): FirebaseFirestore {
        return db
    }

    fun getUID(): String {
        return auth.currentUser!!.uid
    }

    fun addAuthStateListener(listener: FirebaseAuth.AuthStateListener) {
        auth.addAuthStateListener(listener)
    }

    fun removeAuthStateListener(listener: FirebaseAuth.AuthStateListener) {
        auth.removeAuthStateListener(listener)
    }
}