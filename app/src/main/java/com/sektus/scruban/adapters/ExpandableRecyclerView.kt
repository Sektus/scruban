package com.sektus.scruban.adapters

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.sektus.scruban.R
import com.sektus.scruban.databinding.ExpandableRecyclerViewBinding

class ExpandableRecyclerView: LinearLayout {
    private lateinit var binding: ExpandableRecyclerViewBinding
    private var icon: Drawable? = null
    private var expandable = true
    private var buttonIcon: Drawable? = null
    private var title: String? = null
    private var iconTint: Int = 0
    private var buttonTint: Int = 0
    private var expanded = true

    constructor(context: Context): super(context)
    constructor(context: Context, attrs: AttributeSet, defStyle: Int): super(context, attrs, defStyle){
        initView(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet): super(context, attrs){
        initView(context, attrs)
    }

    private fun initView(context: Context, attrs: AttributeSet){
        val inflater = LayoutInflater.from(context)
        //binding = ExpandableRecyclerViewBinding.inflate(inflater, this)
        binding = DataBindingUtil.inflate(inflater, R.layout.expandable_recycler_view, this, true)

        context.theme.obtainStyledAttributes(attrs, R.styleable.ExpandableRecyclerView, 0, 0).apply {
            title = getString(R.styleable.ExpandableRecyclerView_title)
            expandable = getBoolean(R.styleable.ExpandableRecyclerView_expandable, true)
            icon = getDrawable(R.styleable.ExpandableRecyclerView_icon)
            buttonIcon = getDrawable(R.styleable.ExpandableRecyclerView_buttonIcon)
            iconTint = getColor(R.styleable.ExpandableRecyclerView_iconTint, context.getColor(R.color.green))
            buttonTint = getColor(R.styleable.ExpandableRecyclerView_buttonTint, context.getColor(R.color.text))
            recycle()
        }

        binding.headerText.text = title
        orientation = VERTICAL

        if (buttonIcon != null) {
            binding.headerButton.setImageDrawable(buttonIcon)
            binding.headerButton.setColorFilter(buttonTint)
        } else {
            binding.headerButton.visibility = View.GONE
        }

        if (expandable){
            binding.headerExpand.visibility = View.VISIBLE
            binding.header.setOnClickListener {
                tasksToggle()
            }
        } else {
            binding.headerExpand.visibility = View.GONE
        }

        if (icon != null){
            binding.headerIcon.setImageDrawable(icon)
            binding.headerIcon.setColorFilter(iconTint)
        } else {
            binding.headerIcon.visibility = View.GONE
        }
    }

    private fun tasksToggle(){
        if (expanded) {
            binding.headerExpand.animate().setDuration(200).rotation(90.0f)
            expanded = false
            binding.rv.visibility = View.GONE
        } else {
            binding.headerExpand.animate().setDuration(200).rotation(0.0f)
            expanded = true
            binding.rv.visibility = View.VISIBLE
        }
    }

    fun <U, V: ViewDataBinding>setAdapter(adapter: QueryAdapter<U, V>){
        binding.rv.adapter = adapter
    }

    fun setOnButtonClickListener(listener: OnClickListener) {
        binding.headerButton.setOnClickListener(listener)
    }

    fun setOnButtonClickListener( l: ((View) -> (Unit)?) ){
        binding.headerButton.setOnClickListener{ l(it) }
    }

    fun showButton(state: Boolean) {
        if (state) {
            binding.headerButton.visibility = View.VISIBLE
        } else {
            binding.headerButton.visibility = View.GONE
        }
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
    }
}