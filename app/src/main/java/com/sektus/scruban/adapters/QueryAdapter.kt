package com.sektus.scruban.adapters

import android.util.Log
import android.view.Display
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.RecyclerView
import com.sektus.scruban.util.QueryList
import androidx.lifecycle.Observer
import com.sektus.scruban.data.model.Resource
import com.sektus.scruban.data.model.Status

abstract class QueryAdapter<ModelType, BindingType: ViewDataBinding>(
    private val lifecycleOwner: LifecycleOwner,
    private val items: LiveData<Resource<QueryList<ModelType>>>,
    private val layout: Int
): RecyclerView.Adapter<QueryAdapter<ModelType, BindingType>.ViewHolder>() {
    abstract fun bindItem(item:ModelType, binding: BindingType)
    abstract fun onBind(item: ModelType, binding: BindingType)

    init {
        items.observe(lifecycleOwner, Observer {
            Log.d(TAG, "Adapter triggered")
            if (it.status == Status.SUCCESS && it.data != null){
                val idx = it.data.changeIndex
                if (idx != null) {
                    when (it.data.changeType) {
                        QueryList.ChangeType.ADDED -> {
                            notifyItemInserted(idx)
                            notifyItemRangeChanged(idx, itemCount - idx)
                        }
                        QueryList.ChangeType.REMOVED -> {
                            notifyItemRemoved(idx)
                            notifyItemRangeChanged(idx, itemCount - idx)
                        }
                        QueryList.ChangeType.MODIFIED -> notifyItemChanged(idx)
                    }
                } else {
                    notifyDataSetChanged()
                }
            }
        })
    }

    inner class ViewHolder(val binding: BindingType) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: ModelType) {
            bindItem(item, binding)
            binding.lifecycleOwner = lifecycleOwner
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<BindingType>(inflater, layout, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items.value!!.data!!.data[position]
        holder.bind(item)
        onBind(item, holder.binding)
    }

    override fun getItemCount(): Int {
        val value = items.value
        return if (value?.data == null){
            Log.d(TAG, "Size: 0")
            0
        } else {
            Log.d(TAG, "Size: ${value.data.data.size}")
            value.data.data.size
        }
    }

    companion object {
        private val TAG = QueryAdapter::class.java.simpleName
    }
}