package com.sektus.scruban

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.sektus.scruban.data.model.Status
import com.sektus.scruban.viewmodel.UserViewModel


class LoginActivity: AppCompatActivity(){
    private val userViewModel: UserViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        userViewModel.user.observe(this, Observer {
            Log.d(TAG, "Auth state changed")
            if (it.status == Status.SUCCESS && it.data != null){
                val i = Intent(this, MainActivity::class.java)
                startActivity(i)
                finish()
            }
        })

        setContentView(R.layout.activity_login)
    }

    companion object {
        private val TAG = LoginActivity::class.java.simpleName
    }
}