package com.sektus.scruban.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.firestore.*
import com.sektus.scruban.repository.FirebaseRepository
import com.sektus.scruban.data.model.Event

class EventsViewModel : ViewModel() {
    private val noteModifiedIndex = MutableLiveData<Int>()
    private var eventsCollection: CollectionReference = FirebaseRepository.getEventsCollection()
    private val events: MutableList<Event> = mutableListOf()
    private val eventsListener: EventsEventListener = EventsEventListener()

    init {
        loadEvents()
    }

    fun getEvents(): List<Event> {
        return events
    }

    fun getEventModifiedIndex(): LiveData<Int> {
        return noteModifiedIndex
    }

    private fun loadEvents() {
        eventsCollection.addSnapshotListener(eventsListener)
    }

    fun addEvent(item: Event) {
        eventsCollection.add(item).addOnSuccessListener {
            item.id = it.id
        }
    }

    fun removeEvent(item: Event) {
        eventsCollection.document(item.id).delete()
    }

    private inner class EventsEventListener : EventListener<QuerySnapshot> {
        override fun onEvent(snapshot: QuerySnapshot?, error: FirebaseFirestoreException?) {
            if (error != null) {
                //Catch data fetch error
            }
            for (dc in snapshot!!.documentChanges) {
                var eventItem = documentToEventItem(dc.document)
                when (dc.type) {
                    DocumentChange.Type.ADDED -> {
                        events.add(eventItem)
                        noteModifiedIndex.value = events.size - 1
                    }
                    DocumentChange.Type.MODIFIED -> {
                        val idx = findEventIndexById(events, eventItem);
                        if (idx != -1) {
                            events[idx] = eventItem;
                        }
                        noteModifiedIndex.value = idx
                    }
                    DocumentChange.Type.REMOVED -> {
                        val idx = findEventIndexById(events, eventItem);
                        if (idx != -1) {
                            events.removeAt(idx)
                        }
                        noteModifiedIndex.value = idx
                    }
                }
            }
        }
    }


    fun findEventIndexById(list: MutableList<Event>, item: Event): Int {
        var res: Int = -1
        for (i in 0 until list.size) {
            if (list[i].id == item.id) {
                res = i
                break
            }
        }
        return res
    }

    private fun documentToEventItem(item: DocumentSnapshot): Event {
        return Event(
            item.get("title") as String,
            item.get("start") as String,
            item.get("end") as String,
            item.get("color") as String,
            item.id
        )
    }

    companion object {
        val TAG = "EventsViewModel"
    }
}