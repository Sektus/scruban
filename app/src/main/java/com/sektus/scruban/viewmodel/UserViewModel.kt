package com.sektus.scruban.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.sektus.scruban.data.model.*
import com.sektus.scruban.data.repository.NotesRepository
import com.sektus.scruban.data.repository.ProjectRepository
import com.sektus.scruban.data.repository.TodosRepository
import com.sektus.scruban.data.repository.UserRepository
import com.sektus.scruban.util.QueryList
import com.sektus.scruban.util.UserLiveData

class UserViewModel: ViewModel() {
    private val userRepository = UserRepository.getInstance()
    private val todoRepository = TodosRepository.getInstance()
    private val projectsRepository = ProjectRepository.getInstance()
    private val notesRepository = NotesRepository.getInstance()
    private val _note = MutableLiveData<Note>(null)
    val note: LiveData<Note>
        get() = _note

    val user: UserLiveData<User> = userRepository.loadUser()

    val tasks: LiveData<Resource<QueryList<Todo>>> = Transformations.switchMap(user) { res ->
        ifExists(res) { todoRepository.loadTodos(it) }
    }

    val projects: LiveData<Resource<QueryList<Project>>> = Transformations.switchMap(user) { res ->
        ifExists(res) { projectsRepository.loadProjects(it) }
    }

    val notes: LiveData<Resource<QueryList<Note>>> = Transformations.switchMap(user) { res ->
        ifExists(res) { notesRepository.loadNotes(it) }
    }

    fun selectNote(item: Note) {
        if (_note.value == item) {
            return
        }
        _note.value = item
    }

    fun closeNote() {
        saveNote()
    }

    fun saveNote() {
        val note = _note.value
        if (note != null) {
            modifyNote(note)
        }
    }

    private fun requireUser(): User {
        return user.value!!.data!!
    }

    fun createProject(title: String) {
        projectsRepository.createProject(requireUser(), title)
    }

    fun addTodo(title: String) {
        todoRepository.addTodo(requireUser().uid, title)
    }

    fun modifyTodo(todo: Todo) {
        todoRepository.updateTodo(requireUser().uid, todo)
    }

    fun removeTodo(todo: Todo) {
        todoRepository.deleteTodo(requireUser().uid, todo)
    }

    fun addNote(title: String, text: String) {
        notesRepository.createNote(requireUser().uid, title, text)
    }

    fun modifyNote(note: Note) {
        notesRepository.saveNote(requireUser().uid, note)
    }

    fun removeNote(note: Note) {
        notesRepository.deleteNote(requireUser().uid, note)
    }

    fun signUp(nick: String?, email: String?, password: String?): LiveData<Resource<Boolean>> {
        return userRepository.signUp(nick, email, password)
    }

    fun signIn(email: String?, password: String?): LiveData<Resource<Boolean>>{
        return userRepository.signIn(email, password)
    }

    fun signOut(){
        userRepository.signOut()
    }

    fun sendEmailVerification(): LiveData<Resource<Boolean>>{
        return userRepository.sendEmailVerification()
    }

    private fun <T> ifExists(res: Resource<User>?, f: (String) -> LiveData<T>): LiveData<T> {
        return if (res?.data != null) {
            f(res.data.uid)
        } else {
            MutableLiveData<T>()
        }
    }

    companion object {
        private val TAG = UserViewModel::class.java.simpleName
    }
}