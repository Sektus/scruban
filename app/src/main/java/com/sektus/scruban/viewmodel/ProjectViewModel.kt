package com.sektus.scruban.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.google.firebase.Timestamp
import com.sektus.scruban.data.model.History
import com.sektus.scruban.data.model.Resource
import com.sektus.scruban.data.model.TaskItem
import com.sektus.scruban.data.model.User
import com.sektus.scruban.data.repository.ProjectRepository
import com.sektus.scruban.data.repository.TasksRepository
import java.util.*

class ProjectViewModel: ViewModel() {
    private val projectRepository = ProjectRepository.getInstance()
    private val tasksRepository = TasksRepository.getInstance()
    private val _projectId = MutableLiveData<String>(null)
    val projectId: LiveData<String>
        get() = _projectId

    private val _userId = MutableLiveData<String>(null)
    val userId: LiveData<String>
        get() = _userId

    val user = Transformations.switchMap(_projectId) {pid ->
        val uid = _userId.value
        Log.d(TAG, "Loading user")
        if (pid != null && uid != null) {
            Log.d(TAG, "User loaded from repo $uid $pid")
            projectRepository.loadUser(pid, uid)
        } else {
            MutableLiveData<Resource<User>>(Resource.success(null))
        }
    }

    val project = Transformations.switchMap(_projectId) {
        ifExists(it) { id -> projectRepository.loadProject(id) }
    }

    val todoTasks = Transformations.switchMap(_projectId) {
        ifExists(it) { id -> tasksRepository.loadTodoTasks(id) }
    }

    val doingTasks = Transformations.switchMap(_projectId) {
        ifExists(it) { id -> tasksRepository.loadDoingTasks(id) }
    }

    val doneTasks = Transformations.switchMap(_projectId) {
        ifExists(it) { id -> tasksRepository.loadDoneTasks(id) }
    }

    val users = Transformations.switchMap(_projectId) {
        ifExists(it) { id -> projectRepository.loadUsers(id) }
    }
    val allTasks = Transformations.switchMap(_projectId) {
        ifExists(it) { id -> tasksRepository.loadAllTasks(id) }
    }
    val history = Transformations.switchMap(_projectId) {
        ifExists(it) { id -> projectRepository.loadHistory(id) }
    }

    fun onTaskClick(task: TaskItem) {
        Log.d(TAG, "task click ${user.value} ${project.value}")
        tasksRepository.onTaskClick(user.value!!.data!!, project.value!!.data!!, task)
    }

    fun addUser(nick: String) {
        projectRepository.addUser(project.value!!.data!!, nick)
    }

    private fun startTask(nick: String, task: TaskItem) {
        task.started = true
        task.user = nick
        modifyTask(task)
    }

    private fun stopTask(task: TaskItem){
        task.started = false
        val workTime = Timestamp(Date()).seconds - task.updateTime.seconds
        val history = History(workTime,
            task.user!!, task.id, task.title)
        task.user = null
        task.time += workTime

        modifyTask(task)
        addHistory(history)
    }

    fun addHistory(history: History) {
        tasksRepository.addHistory(_projectId.value!!, history)
    }

    fun addTask(title: String) {
        tasksRepository.addTask(projectId.value!!, title)
    }

    fun modifyTask(task: TaskItem) {
        tasksRepository.updateTask(projectId.value!!, task)
    }

    fun removeTask(task: TaskItem) {
        tasksRepository.removeTask(projectId.value!!, task)
    }

    fun setProject(pid: String, uid: String) {
        _projectId.value = pid
        _userId.value = uid
        Log.d(TAG, "user: ${uid} project: ${pid}")
    }

    fun closeProject() {
        _projectId.value = null
        _userId.value = null
    }

    private fun <T> ifExists(id: String?, f: (String) -> LiveData<T>): LiveData<T> {
        return if (id != null) {
            f(id)
        } else {
            MutableLiveData<T>()
        }
    }

    companion object {
        private val TAG = ProjectViewModel::class.java.simpleName
    }
}