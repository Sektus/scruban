package com.sektus.scruban

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.navArgs
import androidx.navigation.ui.setupWithNavController
import com.sektus.scruban.databinding.ActivityProjectBinding
import com.sektus.scruban.repository.FirebaseRepository
import com.sektus.scruban.sevices.TimerService
//import com.sektus.scruban.sevices.TimerService
import com.sektus.scruban.viewmodel.ProjectViewModel

class ProjectActivity: AppCompatActivity() {
    private lateinit var binding: ActivityProjectBinding
    private lateinit var navController: NavController
    private val projectViewModel: ProjectViewModel by viewModels()

    private lateinit var timerService: TimerService
    private var timerBound: Boolean = false
    val args by navArgs<ProjectActivityArgs>()

    private val connection = object: ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            Log.d(TAG, "Service bound")

            val binder = service as TimerService.TimerBinder
            timerService = binder.getSevice()
            timerBound = true
            timerService.setProject(args.projectId)
            timerService.background()
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            Log.d(TAG, "Service disconected")
            timerBound = false
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProjectBinding.inflate(layoutInflater)
        binding.lifecycleOwner = this
        binding.viewmodel = projectViewModel

        val host: NavHostFragment = supportFragmentManager.findFragmentById(R.id.project_nav_host) as NavHostFragment

        navController = host.navController

        //setupActionBarWithNavController(navController)
        binding.projectBottomNavigation.setupWithNavController(navController)
        binding.projectBottomNavigation.setOnNavigationItemReselectedListener {  }

        binding.projectBack.setOnClickListener {
            finish()
        }

        projectViewModel.setProject(args.projectId, args.userId)

        setContentView(binding.root)
    }

    override fun onStart() {
        super.onStart()

        Intent(this, TimerService::class.java).also { intent ->

            startService(intent)
            bindService(intent, connection, Context.BIND_AUTO_CREATE)
        }
    }

    override fun onStop() {
        super.onStop()
        if (timerBound) {
            unbindService(connection)
            timerBound = false
        }
    }

    companion object {
        private val TAG = ProjectActivity::class.java.simpleName
    }
}
