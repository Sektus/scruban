package com.sektus.scruban.sevices

import android.app.*
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.os.Binder
import android.os.Build
import android.os.IBinder
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.lifecycle.MutableLiveData
import com.google.firebase.Timestamp
import com.sektus.scruban.MainActivity
import com.sektus.scruban.R
import com.sektus.scruban.data.model.TaskItem
import java.util.*
import androidx.lifecycle.LifecycleService
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.sektus.scruban.data.model.Resource
import com.sektus.scruban.data.repository.TasksRepository
import com.sektus.scruban.data.repository.UserRepository
import com.sektus.scruban.util.QueryList


class TimerService: LifecycleService() {
    private val tasksRepository = TasksRepository.getInstance()
    private val userRepository = UserRepository.getInstance()

    private val user = userRepository.loadUser()
    private val _projectId = MutableLiveData<String>(null)
    val projectId: LiveData<String>
        get() = _projectId

    fun setProject(pid: String) {
        _projectId.value = pid
    }

    private fun closeProject() {
        _projectId.value = null
    }

    private val activeTask = Transformations.switchMap(user) {
        if (it?.data != null) {
            tasksRepository.getActiveTask(it.data.uid)
        } else {
            MutableLiveData<Resource<TaskItem>>()
        }
    }

    private val doingTasks = Transformations.switchMap(_projectId) {
        if (it != null){
            Log.d(TAG, "Loaded tasks")
            tasksRepository.loadDoingTasks(it)
        } else {
            Log.d(TAG, "Empty tasks")
            MutableLiveData<Resource<QueryList<TaskItem>>>()
        }
    }

    init {
        user.observe(this, androidx.lifecycle.Observer {
            Log.d(TAG, "User $it")
        })

        _projectId.observe(this, androidx.lifecycle.Observer {
            Log.d(TAG, "Project $it")
        })

        doingTasks.observe(this, androidx.lifecycle.Observer { Log.d(TAG, "Tasks triggered") })
        activeTask.observe(this, androidx.lifecycle.Observer { Log.d(TAG, "Active task triggered") })
    }

    private var timer = Timer()

    private var changingConfiguration: Boolean = false
    private var isForeground: Boolean = false
    private lateinit var notificationManager: NotificationManager
    private lateinit var builder: NotificationCompat.Builder

    private val timerTask = object: TimerTask() {
        override fun run() {
            val seconds = Timestamp(Date()).seconds
            if (!isForeground) {
                val tasks = doingTasks.value?.data?.data
                if (tasks != null) {
                    for (task in tasks) {
                        if (task.started) {
                            task.time_text.postValue(TaskItem.timeToString(task.time + seconds - task.updateTime.seconds))

                        }
                    }
                }
            } else {
                val task = activeTask.value?.data
                if (task != null) {
                    task.time_text.postValue(TaskItem.timeToString(task.time + seconds - task.updateTime.seconds))
                    builder.setContentText(task.time_text.value)
                    notificationManager.notify(NOTIFICATION_ID, builder.build())
                }
            }
        }
    }

    private val binder = TimerBinder()

    inner class TimerBinder: Binder() {
        fun getSevice(): TimerService = this@TimerService
    }

    override fun onCreate() {
        super.onCreate()
        Log.d(TAG, "Service created")
        timer.schedule(timerTask, 0, 1000)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)
        Log.d(TAG, "Service started")
        return START_STICKY
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        changingConfiguration = true
    }

    override fun onBind(intent: Intent): IBinder? {
        Log.d(TAG, "Service binding")
        super.onBind(intent)
        background()
        changingConfiguration = false
        return binder
    }

    override fun onRebind(intent: Intent?) {
        background()
        changingConfiguration = false
        super.onRebind(intent)
    }

    override fun onUnbind(intent: Intent?): Boolean {
        if (!changingConfiguration){
            foreground()
            closeProject()
        }
        return true
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "Service destroy")
    }

    fun foreground(){
        if (!isForeground && activeTask.value?.data != null) {
            startForeground(NOTIFICATION_ID, createNotification())
            isForeground = true
        }
    }

    fun background(){
        if (isForeground) {
            stopForeground(true)
            isForeground = false
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(channelId: String, channelName: String) : String {
        val chan = NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_NONE)
        chan.lightColor = getColor(R.color.green)
        chan.lockscreenVisibility = Notification.VISIBILITY_PUBLIC

        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(chan)
        return channelId
    }

    private fun createNotification() : Notification {
        val channelId = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel("TimerService", "Timer Service")
        } else {
            ""
        }
        val task = activeTask.value!!.data!!

        builder = NotificationCompat.Builder(this, channelId)
            .setContentTitle("${task.title} #${task.projectTitle}")
            .setContentText(TaskItem.timeToString(task.time))
            .setSmallIcon(R.mipmap.ic_launcher)
            .setOnlyAlertOnce(true)

        val resultIntent = Intent(this, MainActivity::class.java)
        val resultPendingIntent =
            PendingIntent.getActivity(this, 0, resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT)
        builder.setContentIntent(resultPendingIntent)

        return builder.build()
    }

    companion object {
        private val TAG = "TimerService"
        private val NOTIFICATION_ID = 1
    }
}