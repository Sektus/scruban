package com.sektus.scruban.sevices

import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.*
import com.sektus.scruban.data.model.*

class Webservice {
    private val db = FirebaseFirestore.getInstance()

/* Active tasks */
    fun getActiveTasks(): CollectionReference {
        return db.collection("activeTasks")
    }

    fun getActiveTask(uid: String): DocumentReference {
        return getActiveTasks().document(uid)
    }

    fun setActiveTask(uid: String, task: TaskItem): Task<Void> {
        return getActiveTask(uid).set(task)
    }

    fun deleteActiveTask(uid: String): Task<Void> {
        return getActiveTask(uid).delete()
    }

/* Users */
    fun getUsers(): CollectionReference {
        return db.collection("users")
    }

    fun getUser(uid: String): DocumentReference {
        return getUsers().document(uid)
    }

    fun addUser(user: User): Task<DocumentReference> {
        return getUsers().add(user)
    }

    fun setUser(uid: String, user: User): Task<Void> {
        return getUser(uid).set(user)
    }

    fun deleteUser(user: User): Task<Void> {
        return getUser(user.uid).delete()
    }

/* User todos */
    fun getTodos(uid: String): CollectionReference {
        return getUser(uid).collection("todos")
    }

    fun getTodo(uid: String, tid: String): DocumentReference {
        return getTodos(uid).document(tid)
    }

    fun addTodo(uid: String, todo: Todo): Task<DocumentReference> {
        return getTodos(uid).add(todo)
    }

    fun setTodo(uid: String, todo: Todo): Task<Void> {
        return getTodo(uid, todo.id).set(todo)
    }

    fun deleteTodo(uid: String, todo: Todo): Task<Void> {
        return getTodo(uid, todo.id).delete()
    }

/* User notes */
    fun getNotes(uid: String): CollectionReference {
        return getUser(uid).collection("notes")
    }

    fun getNote(uid: String, nid: String): DocumentReference {
        return getNotes(uid).document(nid)
    }

    fun addNote(uid: String, note: Note): Task<DocumentReference> {
        return getNotes(uid).add(note)
    }

    fun setNote(uid: String, note: Note): Task<Void> {
        return getNote(uid, note.id).set(note)
    }

    fun deleteNote(uid: String, note: Note): Task<Void> {
        return getNote(uid, note.id).delete()
    }

/* User projects */
    fun getUserProjects(uid: String): CollectionReference {
        return getUser(uid).collection("projects")
    }

    fun getUserProject(uid: String, pid: String): DocumentReference {
        return getUserProjects(uid).document(pid)
    }

    fun addUserProject(uid: String, project: Project): Task<Void> {
        return setUserProject(uid, project)
    }

    fun setUserProject(uid: String, project: Project): Task<Void> {
        return getUserProject(uid, project.id).set(project)
    }

    fun deleteUserProject(uid: String, pid: String): Task<Void> {
        return getUserProject(uid, pid).delete()
    }

/* Projects */
    fun getProjects(): CollectionReference {
        return db.collection("projects")
    }

    fun getProject(pid: String): DocumentReference {
        return getProjects().document(pid)
    }

    fun addProject(project: Project): Task<DocumentReference> {
        return getProjects().add(project)
    }

    fun deleteProject(pid: String): Task<Void> {
        return getProject(pid).delete()
    }

    fun setProject(project: Project): Task<Void> {
        return getProject(project.id).set(project)
    }

/* Project Tasks */
    fun getProjectTasks(pid: String): CollectionReference {
        return getProject(pid).collection("tasks")
    }

    fun getProjectTask(pid: String, tid: String): DocumentReference {
        return getProjectTasks(pid).document(tid)
    }

    fun addProjectTask(pid: String, task: TaskItem): Task<DocumentReference> {
        return getProjectTasks(pid).add(task)
    }

    fun deleteProjectTask(pid: String, tid: String): Task<Void> {
        return getProjectTask(pid, tid).delete()
    }

    fun setProjectTask(pid: String, task: TaskItem) : Task<Void> {
        return getProjectTask(pid, task.id).set(task)
    }

/* Project history */
    fun getProjectHistory(pid: String): CollectionReference {
        return getProject(pid).collection("history")
    }

    fun getProjectHistoryItem(pid: String, hid: String): DocumentReference {
        return getProjectHistory(pid).document(hid)
    }

    fun addProjectHistory(pid: String, history: History): Task<DocumentReference> {
        return getProjectHistory(pid).add(history)
    }

/* Project users */
    fun getProjectUsers(pid: String): CollectionReference {
        return getProject(pid).collection("users")
    }

    fun getProjectUser(pid: String, uid: String): DocumentReference {
        return getProjectUsers(pid).document(uid)
    }

    fun addProjectUser(pid: String, user: User): Task<Void> {
        return setProjectUser(pid, user)//getProjectUsers(pid).add(user)
    }

    fun deleteProjectUser(pid: String, uid: String): Task<Void> {
        return getProjectUser(pid, uid).delete()
    }

    fun setProjectUser(pid: String, user: User): Task<Void> {
        return getProjectUser(pid, user.uid).set(user)
    }

    fun <T> runTransaction(f: (Transaction) -> (T?)): Task<T> {
        return db.runTransaction(f)
    }

    companion object {
        private val TAG = Webservice::class.java.simpleName
        private var INSTANCE: Webservice? = null

        fun getInstance(): Webservice =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildInstance().also { INSTANCE = it }
            }

        private fun buildInstance() = Webservice()
    }
}