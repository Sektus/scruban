package com.sektus.scruban.ui.project

import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.sektus.scruban.R
import com.sektus.scruban.adapters.QueryAdapter
import com.sektus.scruban.data.model.Status
import com.sektus.scruban.data.model.User
import com.sektus.scruban.databinding.FragmentUsersBinding
import com.sektus.scruban.databinding.ItemUserBinding
import com.sektus.scruban.viewmodel.ProjectViewModel

class UsersFragment : Fragment(){
    private lateinit var usersAdapter: QueryAdapter<User, ItemUserBinding>
    private lateinit var binding: FragmentUsersBinding
    private val projectViewModel: ProjectViewModel by activityViewModels()
    private lateinit var user: User

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentUsersBinding.inflate(inflater)

        projectViewModel.users.observe(viewLifecycleOwner, Observer {
            if (it.status == Status.SUCCESS && it.data != null) {
                usersAdapter = object: QueryAdapter<User, ItemUserBinding>
                    (viewLifecycleOwner, projectViewModel.users, R.layout.item_user){
                    override fun onBind(item: User, binding: ItemUserBinding) {
                        binding.userItemRoot.setOnLongClickListener {
                            onUserLongClick(item, it)
                            true
                        }
                    }

                    override fun bindItem(item: User, binding: ItemUserBinding) {
                        binding.item = item
                    }
                }
                binding.usersRv.setAdapter(usersAdapter)
            }
        })

        projectViewModel.user.observe(viewLifecycleOwner, Observer {
            if (it?.data != null) {
                user = it.data
                if (user.role == "Maintainer"){
                    binding.usersRv.showButton(true)
                    binding.usersRv.setOnButtonClickListener {
                        addNewUserDialog()
                    }
                } else {
                    binding.usersRv.showButton(false)
                }
            }
        })

        return binding.root
    }

    fun onUserLongClick(user: User, view: View) {
        //TODO()
    }

    private fun addNewUserDialog(){
        var editNickname: EditText
        val dialog: AlertDialog? = this.activity.let {
            val builder = AlertDialog.Builder(it)
            editNickname = EditText(it)
            editNickname.hint = "Nickname"

            builder.apply {
                setPositiveButton("Add", null)
                setNegativeButton("Cancel") { dialog, id ->
                    Log.w("NewTask", "User canceled task creation")
                }
                setTitle("Add new user")
                setView(editNickname)
            }

            builder.create()
        }

        dialog?.setOnShowListener { it ->
            val button = dialog.getButton(AlertDialog.BUTTON_POSITIVE)
            button.setOnClickListener {
                if (editNickname.text.isNotEmpty()){
                    val nick = editNickname.text.toString()
                    projectViewModel.addUser(nick)
                    dialog.dismiss()
                } else {
                    editNickname.error = "Nickname required"
                }
            }
        }
        dialog?.show()
    }
}