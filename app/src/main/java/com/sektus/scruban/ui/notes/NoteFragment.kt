package com.sektus.scruban.ui.notes

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.sektus.scruban.R
import com.sektus.scruban.databinding.FragmentNoteBinding
import com.sektus.scruban.viewmodel.UserViewModel


class NoteFragment: Fragment() {
    private val userViewModel: UserViewModel by activityViewModels()
    private lateinit var binding: FragmentNoteBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requireActivity().onBackPressedDispatcher.addCallback(this) {
            findNavController().navigate(
                NoteFragmentDirections.closeNote()
            )
            userViewModel.closeNote()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_note, container, false)
        binding.item = userViewModel.note.value
        binding.noteSave.setOnClickListener {
            userViewModel.saveNote()
        }
        return binding.root
    }
}