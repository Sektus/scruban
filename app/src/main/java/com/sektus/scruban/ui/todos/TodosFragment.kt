package com.sektus.scruban.ui.todos

import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.PopupMenu
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.sektus.scruban.R
import com.sektus.scruban.adapters.QueryAdapter
import com.sektus.scruban.data.model.Project
import com.sektus.scruban.data.model.Status
import com.sektus.scruban.data.model.Todo
import com.sektus.scruban.databinding.FragmentTodosBinding
import com.sektus.scruban.databinding.ItemProjectBinding
import com.sektus.scruban.databinding.ItemTodoBinding
import com.sektus.scruban.viewmodel.UserViewModel

class TodosFragment: Fragment() {
    private lateinit var binding: FragmentTodosBinding
    private lateinit var todosAdapter: QueryAdapter<Todo, ItemTodoBinding>
    private lateinit var projectsAdapter: QueryAdapter<Project, ItemProjectBinding>
    private val userViewModel: UserViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentTodosBinding.inflate(inflater)

        userViewModel.tasks.observe(viewLifecycleOwner, Observer {
            if (it.status == Status.SUCCESS && it.data != null) {
                todosAdapter = object: QueryAdapter<Todo, ItemTodoBinding>
                    (viewLifecycleOwner, userViewModel.tasks, R.layout.item_todo){
                    override fun onBind(item: Todo, binding: ItemTodoBinding) {
                        binding.todoItemRoot.setOnClickListener {
                            onTodoClick(item)
                        }

                        binding.todoItemRoot.setOnLongClickListener {
                            onTodoLongClick(item, it)
                            true
                        }
                    }

                    override fun bindItem(item: Todo, binding: ItemTodoBinding) {
                        binding.item = item
                    }
                }
                binding.todosRv.setAdapter(todosAdapter)
            }
        })

        userViewModel.projects.observe(viewLifecycleOwner, Observer {
            if (it.status == Status.SUCCESS && it.data != null) {
                projectsAdapter = object: QueryAdapter<Project, ItemProjectBinding>
                    (viewLifecycleOwner, userViewModel.projects, R.layout.item_project){
                    override fun onBind(item: Project, binding: ItemProjectBinding) {
                        binding.projectItemRoot.setOnClickListener {
                            onProjectClick(item)
                        }
                    }

                    override fun bindItem(item: Project, binding: ItemProjectBinding) {
                        binding.item = item
                    }
                }

                binding.projectsRv.setAdapter(projectsAdapter)
            }
        })
        binding.todosRv.setOnButtonClickListener {
            addTodoDialog()
        }

        binding.projectsRv.setOnButtonClickListener {
            createProjectDialog()
        }

        return binding.root
    }

    fun onProjectClick(item: Project) {
        findNavController().navigate(
            TodosFragmentDirections.openProject(item.id, userViewModel.user.value!!.data!!.uid)
        )
    }

    fun onTodoClick(item: Todo) {
        item.status = !item.status
        userViewModel.modifyTodo(item)
    }

    fun onTodoLongClick(item: Todo, view: View){
        val menu = PopupMenu(context, view, Gravity.END)
        menu.inflate(R.menu.todo_menu)
        menu.setOnMenuItemClickListener {
            when(it.itemId){
                R.id.todo_edit -> {
                    editTodoDialog(item)
                    true
                }
                R.id.todo_delete -> {
                    userViewModel.removeTodo(item)
                    true
                }
                else -> false
            }
        }
        menu.show()
    }

    private fun addTodoDialog() {
        val dialog: AlertDialog? = this.activity.let {
            val builder = AlertDialog.Builder(it)
            val editTitle = EditText(it)
            editTitle.hint = "Todo title"

            builder.apply {
                setPositiveButton("Add") { dialog, id ->
                    if (editTitle.text.isNotEmpty()){
                        val title = editTitle.text.toString()
                        userViewModel.addTodo(title)
                    }
                }
                setNegativeButton("Cancel") { dialog, id ->
                    Log.w("NewTask", "User canceled task creation")
                }
                setTitle("Create todo")
                setView(editTitle)
            }

            builder.create()
        }
        dialog?.show()
    }
    private fun createProjectDialog() {
        val dialog: AlertDialog? = this.activity.let {
            val builder = AlertDialog.Builder(it)
            val editTitle = EditText(it)
            editTitle.hint = "Project title"

            builder.apply {
                setPositiveButton("Create") { dialog, id ->
                    if (editTitle.text.isNotEmpty()){
                        val title = editTitle.text.toString()
                        userViewModel.createProject(title)
                    }
                }
                setNegativeButton("Cancel") { _, _ ->
                    Log.w("Project", "User canceled project creation")
                }
                setTitle("Create new project")
                setView(editTitle)
            }

            builder.create()
        }
        dialog?.show()
    }

    private fun editTodoDialog(todo: Todo){
        val dialog: AlertDialog? = this.activity.let {
            val builder = AlertDialog.Builder(it)
            val editTitle = EditText(it)
            editTitle.setText(todo.title)

            builder.apply {
                setPositiveButton("Save") { _, _ ->
                    if (editTitle.text.isNotEmpty()){
                        todo.title = editTitle.text.toString()
                        userViewModel.modifyTodo(todo)
                    }
                }
                setNegativeButton("Cancel") { _, _ ->
                    Log.w("NewTask", "User canceled task creation")
                }
                setTitle("Modify todo")
                setView(editTitle)
            }

            builder.create()
        }
        dialog?.show()
    }
}