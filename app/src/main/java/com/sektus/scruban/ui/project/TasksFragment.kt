package com.sektus.scruban.ui.project

import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.EditText
import android.widget.PopupMenu
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.sektus.scruban.R
import androidx.lifecycle.Observer
import com.sektus.scruban.adapters.QueryAdapter
import com.sektus.scruban.data.model.Status
import com.sektus.scruban.data.model.TaskItem
import com.sektus.scruban.data.model.User
import com.sektus.scruban.databinding.*
import com.sektus.scruban.viewmodel.ProjectViewModel

class TasksFragment : Fragment(){
    private val projectViewModel: ProjectViewModel by activityViewModels()

    private lateinit var binding: FragmentTasksBinding
    private lateinit var todoAdapter: QueryAdapter<TaskItem, ItemTaskBinding>
    private lateinit var doingAdapter: QueryAdapter<TaskItem, ItemTaskBinding>
    private lateinit var doneAdapter: QueryAdapter<TaskItem, ItemTaskBinding>
    private lateinit var user: User

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentTasksBinding.inflate(inflater)

        projectViewModel.todoTasks.observe(viewLifecycleOwner, Observer {
            if (it.status == Status.SUCCESS && it.data != null) {
                todoAdapter = object: QueryAdapter<TaskItem, ItemTaskBinding>
                    (viewLifecycleOwner, projectViewModel.todoTasks, R.layout.item_task){
                    override fun onBind(item: TaskItem, binding: ItemTaskBinding) {
                        binding.tasksItemRoot.setOnClickListener {
                            onTodoTaskClick(item)
                        }

                        binding.tasksItemRoot.setOnLongClickListener {
                            onTaskLongClick(item, it)
                            true
                        }
                    }

                    override fun bindItem(item: TaskItem, binding: ItemTaskBinding) {
                        binding.item = item
                    }
                }
                binding.todoRv.setAdapter(todoAdapter)
            }
        })

        projectViewModel.doingTasks.observe(viewLifecycleOwner, Observer {
            if (it.status == Status.SUCCESS && it.data != null) {
                doingAdapter = object: QueryAdapter<TaskItem, ItemTaskBinding>
                    (viewLifecycleOwner, projectViewModel.doingTasks, R.layout.item_task){
                    override fun onBind(item: TaskItem, binding: ItemTaskBinding) {
                        binding.tasksItemRoot.setOnClickListener {
                            onDoingTaskClick(item)
                        }

                        binding.tasksItemRoot.setOnLongClickListener {
                            onTaskLongClick(item, it)
                            true
                        }
                    }

                    override fun bindItem(item: TaskItem, binding: ItemTaskBinding) {
                        binding.item = item
                    }
                }
                binding.doingRv.setAdapter(doingAdapter)
            }
        })

        projectViewModel.doneTasks.observe(viewLifecycleOwner, Observer {
            if (it.status == Status.SUCCESS && it.data != null) {
                doneAdapter = object: QueryAdapter<TaskItem, ItemTaskBinding>
                    (viewLifecycleOwner, projectViewModel.doneTasks, R.layout.item_task){
                    override fun onBind(item: TaskItem, binding: ItemTaskBinding) {
                        binding.tasksItemRoot.setOnLongClickListener {
                            onTaskLongClick(item, it)
                            true
                        }
                    }

                    override fun bindItem(item: TaskItem, binding: ItemTaskBinding) {
                        binding.item = item
                    }
                }
                binding.doneRv.setAdapter(doneAdapter)
            }
        })

        binding.todoRv.setOnButtonClickListener {
            addNewTaskDialog()
        }

        projectViewModel.user.observe(viewLifecycleOwner, Observer {
            if (it?.data != null) {
                user = it.data
            }
        })

        return binding.root
    }

    fun onTaskLongClick(item: TaskItem, view: View) {
        val menu = PopupMenu(context, view, Gravity.END)
        Log.d(TAG, "User: $user")
        if (user.role == "Maintainer"){
            if (item.status == "doing" && !item.started) {
                menu.menu.add(Menu.NONE, R.id.mark_done, 0, "Mark as completed")
            }
            if (!item.started && item.status != "todo") {
                menu.menu.add(Menu.NONE, R.id.task_delete, 2, "Delete")
            }
        }
        if (item.status == "todo"){
            menu.menu.add(Menu.NONE, R.id.task_delete, 2, "Delete")
        }
        if (item.status == "todo") {
            menu.menu.add(Menu.NONE, R.id.task_edit, 1, "Edit")
        }
        menu.show()

        menu.setOnMenuItemClickListener { menuItem ->
            when (menuItem.itemId) {
                R.id.mark_done -> {
                    item.status = "done"
                    projectViewModel.modifyTask(item)
                    true
                }
                R.id.task_edit -> {
                    editTaskDialog(item)
                    true
                }
                R.id.task_delete -> {
                    projectViewModel.removeTask(item)
                    true
                }
                else -> false
            }
        }
    }

    fun onTodoTaskClick(item: TaskItem) {
        item.status = "doing"
        onDoingTaskClick(item)
    }

    fun onDoingTaskClick(item: TaskItem) {
        projectViewModel.onTaskClick(item)
    }


    private fun addNewTaskDialog(){
        val dialog: AlertDialog? = this.activity.let {
            val builder = AlertDialog.Builder(it)
            val editTitle = EditText(it)
            editTitle.hint = "Task title"

            builder.apply {
                setPositiveButton("Add") { dialog, id ->
                    if (editTitle.text.isNotEmpty()){
                        projectViewModel.addTask(editTitle.text.toString())
                    }
                    Log.w("NewTask", "User created task ${editTitle.text}")
                }
                setNegativeButton("Cancel") { dialog, id ->
                    Log.w("NewTask", "User canceled task creation")
                }
                setTitle("Add new task")
                setView(editTitle)
            }

            builder.create()
        }
        dialog?.show()
    }

    private fun editTaskDialog(item: TaskItem){
        val dialog: AlertDialog? = this.activity.let {
            val builder = AlertDialog.Builder(it)
            val editTitle = EditText(it)
            editTitle.setText(item.title)

            builder.apply {
                setPositiveButton("Edit") { dialog, id ->
                    if (editTitle.text.isNotEmpty()){
                        item.title = editTitle.text.toString()
                        projectViewModel.modifyTask(item)
                    }
                    Log.w("NewTask", "User created task ${editTitle.text}")
                }
                setNegativeButton("Cancel") { dialog, id ->
                    Log.w("NewTask", "User canceled task creation")
                }
                setTitle("Edit task")
                setView(editTitle)
            }

            builder.create()
        }
        dialog?.show()
    }

    companion object {
        private val TAG = TasksFragment::class.java.simpleName
    }
}