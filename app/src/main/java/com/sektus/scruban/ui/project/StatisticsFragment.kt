package com.sektus.scruban.ui.project

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.charts.HorizontalBarChart
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.utils.ColorTemplate
import com.sektus.scruban.R
import com.sektus.scruban.data.model.History
import com.sektus.scruban.data.model.Status
import com.sektus.scruban.data.model.TaskItem
import com.sektus.scruban.databinding.FragmentStatisticsBinding
import com.sektus.scruban.viewmodel.ProjectViewModel


class StatisticsFragment : Fragment() {
    private lateinit var tasksTimeChart: BarChart
    private lateinit var tasksStatusesChart: HorizontalBarChart
    private lateinit var userParticipationChart: PieChart
    private lateinit var binding: FragmentStatisticsBinding
    private val projectViewModel: ProjectViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_statistics, container, false
        )
        tasksTimeChart = binding.tasksTimeChart
        userParticipationChart = binding.userParticipationChart
        tasksStatusesChart = binding.tasksStatusesChart

        projectViewModel.allTasks.observe(viewLifecycleOwner, Observer {
            if (it.status == Status.SUCCESS && it.data != null) {
                redrawBarChart(it.data.data)
                redrawStatusesChart(it.data.data)
            }
        })

        projectViewModel.history.observe(viewLifecycleOwner, Observer {
            if (it.status == Status.SUCCESS && it.data != null) {
                redrawPieChart(it.data.data)
            }
        })

        return binding.root
    }

    private fun redrawBarChart(tasks: List<TaskItem>) {
        val data = BarData()

        for (i in tasks.indices) {
            val minutes: Float = tasks[i].time.toFloat() / 60
            val dataSet =
                BarDataSet(
                    mutableListOf(BarEntry(i.toFloat(), minutes)),
                    tasks[i].title
                )
            dataSet.color = ColorTemplate.COLORFUL_COLORS[i % 5]
            data.addDataSet(dataSet)
        }
        data.barWidth = 0.9f // set custom bar width
        data.setValueTextSize(14f)
        tasksTimeChart.data = data
        // customization
        tasksTimeChart.setFitBars(true) // make the x-axis fit exactly all bars
        tasksTimeChart.xAxis.setDrawLabels(false)
        tasksTimeChart.xAxis.setDrawAxisLine(false)
        tasksTimeChart.xAxis.setDrawGridLines(false)
        tasksTimeChart.xAxis.setDrawGridLinesBehindData(false)
        tasksTimeChart.axisLeft.textSize = 16f
        tasksTimeChart.axisLeft.axisMinimum = 0f
        tasksTimeChart.axisRight.isEnabled = false
        tasksTimeChart.legend.textSize = 16f
        tasksTimeChart.legend.xEntrySpace = 7f
        tasksTimeChart.legend.form = Legend.LegendForm.CIRCLE
        tasksTimeChart.legend.formSize = 16f
        tasksTimeChart.description.isEnabled = false

        // refresh
        tasksTimeChart.invalidate()
    }

    private fun redrawPieChart(histories: List<History>) {
        val userTimes: HashMap<String, Float> = HashMap()
        val entries: MutableList<PieEntry> = mutableListOf()

        for (history in histories) {
            val user: String = history.user
            var time: Float = history.time.toFloat()
            Log.d("HISTORIES", "User: $user\ttime: $time")
            if (userTimes.containsKey(user)) {
                time += userTimes[user]!!
            }
            userTimes[user] = time
        }

        for (user in userTimes.keys) {
            entries.add(PieEntry(userTimes[user]!!, user))
        }

        val dataSet = PieDataSet(entries, "")
        dataSet.colors = ColorTemplate.MATERIAL_COLORS.toList()
        val data = PieData(dataSet)
        data.setValueTextSize(16f)
        data.setValueTextColor(Color.BLACK)
        userParticipationChart.data = data
        userParticipationChart.legend.textSize = 16f
        userParticipationChart.legend.xEntrySpace = 7f
        userParticipationChart.legend.form = Legend.LegendForm.CIRCLE
        userParticipationChart.legend.formSize = 16f
        userParticipationChart.description.isEnabled = false
        userParticipationChart.setUsePercentValues(true)
        userParticipationChart.holeRadius = 10f
        userParticipationChart.transparentCircleRadius = 25f
        userParticipationChart.setEntryLabelTextSize(16f)
        userParticipationChart.setEntryLabelColor(Color.BLACK)

        data.setValueFormatter(object : ValueFormatter() {
            override fun getFormattedValue(value: Float): String {
                return "%.${2}f".format(value) + "%"
            }
        })

        // refresh
        userParticipationChart.invalidate()
    }

    private fun redrawStatusesChart(tasks: List<TaskItem>) {
        val data = BarData()
        val taskStatusesCount: HashMap<String, Int> = HashMap()

        for (task in tasks) {
            val status: String = task.status
            var count = 1
            if (taskStatusesCount.containsKey(status)) {
                count += taskStatusesCount[status]!!
            }
            taskStatusesCount[status] = count
        }

        for ((i, status) in taskStatusesCount.keys.withIndex()) {
            val dataSet =
                BarDataSet(
                    mutableListOf(BarEntry(i.toFloat(), taskStatusesCount[status]!!.toFloat())),
                    status
                )
            dataSet.color = ColorTemplate.COLORFUL_COLORS[i % 5]
            data.addDataSet(dataSet)
        }

        data.barWidth = 0.9f // set custom bar width
        data.setValueTextSize(14f)
        tasksStatusesChart.data = data
        // customization
        tasksStatusesChart.setFitBars(true) // make the x-axis fit exactly all bars
        tasksStatusesChart.xAxis.setDrawLabels(false)
        tasksStatusesChart.xAxis.setDrawAxisLine(false)
        tasksStatusesChart.xAxis.setDrawGridLines(false)
        tasksStatusesChart.xAxis.setDrawGridLinesBehindData(false)
        tasksStatusesChart.axisLeft.textSize = 16f
        tasksStatusesChart.axisLeft.axisMinimum = 0f
        tasksStatusesChart.axisRight.isEnabled = false
        tasksStatusesChart.legend.textSize = 16f
        tasksStatusesChart.legend.xEntrySpace = 7f
        tasksStatusesChart.legend.form = Legend.LegendForm.CIRCLE
        tasksStatusesChart.legend.formSize = 16f
        tasksStatusesChart.description.isEnabled = false

        // refresh
        tasksStatusesChart.invalidate()
    }
}