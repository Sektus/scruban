package com.sektus.scruban.ui.login

import android.app.Activity
import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.sektus.scruban.R
import com.sektus.scruban.data.model.Status
import com.sektus.scruban.data.model.User
import com.sektus.scruban.databinding.FragmentLoginBinding
import com.sektus.scruban.util.UserLiveData
import com.sektus.scruban.viewmodel.UserViewModel


class LoginFragment: Fragment(), View.OnClickListener{
    private lateinit var binding: FragmentLoginBinding
    private var errorFlag: Boolean = false
    private val userViewModel: UserViewModel by activityViewModels()
    private lateinit var user: UserLiveData<User>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_login, container, false)

        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewmodel = userViewModel

        //set buttons listeners to this
        binding.loginSignIn.setOnClickListener(this)
        binding.loginSignUp.setOnClickListener(this)

        user = userViewModel.user

        user.observe(viewLifecycleOwner, Observer {
            if (it.status == Status.ERROR){
                if (it.message!! == "Email is not verified"){
                    sendEmailVerificationDialog()
                }
            }
        })
        return binding.root
    }

    private fun signIn(){
        Log.d(TAG, "signIn:pressed")
        binding.resource = userViewModel.signIn(binding.email, binding.password)
    }

    private fun signUp(){
        findNavController().navigate(
            LoginFragmentDirections.showSignUp()
        )
    }

    private fun sendEmailVerificationDialog() {
        val dialog: AlertDialog? = this.context.let {
            val builder = AlertDialog.Builder(it)

            builder.apply {
                setPositiveButton(getString(R.string.yes_button)) { dialog, id ->
                    binding.resource = userViewModel.sendEmailVerification()
                }
                setNegativeButton(getString(R.string.no_button)) { dialog, id ->
                    userViewModel.signOut()
                    dialog.cancel()
                }
                setMessage(getString(R.string.verification_dialog))
            }

            builder.create()
        }
        dialog?.show()
    }

    override fun onClick(v: View) {
        hideKeyboard()
        when (v.id) {
            binding.loginSignIn.id -> signIn()
            binding.loginSignUp.id -> signUp()
        }
    }

    private fun hideKeyboard() {
        val context = this.requireContext()
        val window = this.view?.rootView?.windowToken
        val imm = context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(window, 0)
    }

    companion object {
        private val TAG = this::class.java.simpleName
    }
}