package com.sektus.scruban.ui.notes

import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.PopupMenu
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.sektus.scruban.R
import com.sektus.scruban.adapters.QueryAdapter
import com.sektus.scruban.data.model.Note
import com.sektus.scruban.data.model.Status
import com.sektus.scruban.databinding.FragmentNotesBinding
import com.sektus.scruban.databinding.ItemNoteBinding
import com.sektus.scruban.viewmodel.UserViewModel

class NotesFragment : Fragment() {
    private lateinit var notesAdapter: QueryAdapter<Note, ItemNoteBinding>
    private lateinit var binding: FragmentNotesBinding
    private val userViewModel: UserViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_notes, container, false
        )

        userViewModel.notes.observe(viewLifecycleOwner, Observer {
            if (it.status == Status.SUCCESS && it.data != null) {
                notesAdapter = object: QueryAdapter<Note, ItemNoteBinding>
                    (viewLifecycleOwner, userViewModel.notes, R.layout.item_note){
                    override fun onBind(item: Note, binding: ItemNoteBinding) {
                        binding.noteItemRoot.setOnClickListener {
                            onNoteClick(item)
                        }

                        binding.noteItemRoot.setOnLongClickListener {
                            onNoteLongClick(item, it)
                            true
                        }
                    }

                    override fun bindItem(item: Note, binding: ItemNoteBinding) {
                        binding.item = item
                    }
                }
                binding.notesRv.setAdapter(notesAdapter)
            }
        })

        binding.notesRv.setOnButtonClickListener {
            addNewNoteDialog()
        }



        return binding.root
    }

    private fun addNewNoteDialog() {
        val dialog: AlertDialog? = this.activity.let {
            val builder = AlertDialog.Builder(it)
            val editTitle = EditText(it)
            editTitle.hint = "Note title"
            editTitle.setSingleLine()
            val editText = EditText(it)
            editText.hint = "Note text"
            var layout = LinearLayout(it)
            layout.orientation = LinearLayout.VERTICAL
            layout.addView(editTitle)
            layout.addView(editText)

            builder.apply {
                setPositiveButton("Add") { dialog, id ->
                    if (editTitle.text.isNotEmpty() && editText.text.isNotEmpty()) {
                        val title = editTitle.text.toString()
                        val text = editText.text.toString()
                        userViewModel.addNote(title, text)
                    }
                }
                setNegativeButton("Cancel") { dialog, id ->
                    Log.w("NewNote", "User canceled note creation")
                }
                setTitle("Add new note")
                setView(layout)
            }

            builder.create()
        }
        dialog?.show()
    }

    fun onNoteLongClick(item: Note, view: View) {
        val menu = PopupMenu(context, view, Gravity.END)
        menu.inflate(R.menu.note_menu)
        menu.setOnMenuItemClickListener {
            when(it.itemId){
                R.id.note_delete -> {
                    userViewModel.removeNote(item)
                    true
                }
                else -> false
            }
        }
        menu.show()
    }

    fun onNoteClick(note: Note) {
        userViewModel.selectNote(note)
        findNavController().navigate(NotesFragmentDirections.openNote())
    }
}
