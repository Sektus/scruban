package com.sektus.scruban.com.sektus.scruban

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.graphics.Color
import android.icu.text.SimpleDateFormat
import android.icu.util.Calendar
import android.os.Bundle
import android.text.Editable
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.sektus.scruban.R
import com.sektus.scruban.data.model.Event
import com.sektus.scruban.viewmodel.EventsViewModel
import kotlinx.android.synthetic.main.new_calendar_event.view.*
import java.time.LocalDate
import java.time.ZoneId
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.abs

class CalendarFragment : Fragment() {
    private lateinit var calendarView: LinearLayout
    private lateinit var monthTitle: TextView
    private lateinit var btnNext: ImageView
    private lateinit var btnPrevious: ImageView
    private var widthOfText: Int = 0

    private val daysRows: ArrayList<View> = ArrayList(6)
    private var calendar: Calendar = Calendar.getInstance()
    private val dateFormatter = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
    private val model: EventsViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_calendar, container, false)
        monthTitle = root.findViewById(R.id.txt_monthTitle) as TextView
        btnNext = root.findViewById(R.id.btn_next) as ImageView
        btnPrevious = root.findViewById(R.id.btn_previous) as ImageView

        calendarView = root!!.findViewById(R.id.calendar_view)
        calendarView.removeAllViews()
        for (i in 0 until 6) {
            daysRows.add(inflater.inflate(R.layout.days_row, container, false))
            if (daysRows[i].parent != null) {
                (daysRows[i].parent as ViewGroup).removeView(daysRows[i])
            }
            calendarView.addView(daysRows[i])
        }

        monthTitle.text =
            SimpleDateFormat(monthYearFormat, Locale.getDefault()).format(calendar.time)
        btnNext.setOnClickListener { nextMonth() }
        btnPrevious.setOnClickListener { previousMonth() }

        val btn: ImageButton? = root.findViewById(R.id.addEventBtn)
        btn?.setOnClickListener {
            //Show Dialog here to add new Item
            addNewItemDialog()
        }

        model.getEventModifiedIndex().observe(viewLifecycleOwner, Observer<Int> {
            updateEventView()
        })


        root.post { // for instance
            var day1 = daysRows[0].findViewById(R.id.day1) as TextView
            widthOfText = (daysRows[0].measuredWidth / 7)
            updateEventView()
        }

        return root
    }

    private fun addNewItemDialog() {
        val dialogView = layoutInflater.inflate(R.layout.new_calendar_event, null)
        val alert =
            AlertDialog.Builder(activity).setView(dialogView).setTitle("Add New Event").show()

        dialogView.startDateBox.setOnClickListener {
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)
            val dpd = DatePickerDialog(
                requireContext(),
                DatePickerDialog.OnDateSetListener { _, year, month, day ->
                    dialogView.startDateBox.text =
                        Editable.Factory.getInstance().newEditable(
                            day.toString().padStart(2, '0') + "-" + (month + 1).toString()
                                .padStart(2, '0') + "-" + year.toString()
                        )
                },
                year,
                month,
                day
            )
            dpd.show()
        }
        dialogView.endDateBox.setOnClickListener {
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)
            val dpd = DatePickerDialog(
                requireContext(),
                DatePickerDialog.OnDateSetListener { view, year, month, day ->
                    dialogView.endDateBox.text =
                        Editable.Factory.getInstance().newEditable(
                            day.toString().padStart(2, '0') + "-" + (month + 1).toString()
                                .padStart(2, '0') + "-" + year.toString()
                        )
                },
                year,
                month,
                day
            )
            dpd.show()
        }
        dialogView.addEventBtn.setOnClickListener {
            val startDate: LocalDate =
                dateToLocalDate(dateFormatter.parse(dialogView.startDateBox.text.toString()))
            val endDate: LocalDate =
                dateToLocalDate(dateFormatter.parse(dialogView.endDateBox.text.toString()))
            if (startDate <= endDate) {
                val eventItem = Event()
                eventItem.title = dialogView.eventTitle.text.toString()
                eventItem.start = dialogView.startDateBox.text.toString()
                eventItem.end = dialogView.endDateBox.text.toString()
                eventItem.color =
                    "#" + Integer.toHexString(dialogView.color_picker_view.selectedColor)
                model.addEvent(eventItem)
                alert.dismiss()
                Toast.makeText(
                    activity,
                    "Event Item saved color: " + eventItem.color,
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                alert.dismiss()
                Toast.makeText(
                    activity,
                    "Choose correct date!",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
        dialogView.cancelAddEventDialogBtn.setOnClickListener {
            alert.dismiss()
            Toast.makeText(
                activity,
                "Dismissed event add dialog!",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun nextMonth() {
        calendar.add(Calendar.MONTH, 1)
        monthTitle.text =
            SimpleDateFormat(monthYearFormat, Locale.getDefault()).format(calendar.time)
        updateEventView()
    }

    private fun previousMonth() {
        calendar.add(Calendar.MONTH, -1)
        monthTitle.text =
            SimpleDateFormat(monthYearFormat, Locale.getDefault()).format(calendar.time)
        updateEventView()
    }

    private fun updateEventView() {
        removeAllEventStripes()
        var selectedCalendar: Calendar = Calendar.getInstance()
        //This will get current calender time of system
        selectedCalendar.set(
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DATE)
        )
        //This will set selectedCalendar to selected month and year by next/previous buttons
        //This will set date to first
        selectedCalendar.set(Calendar.DATE, 1)
        selectedCalendar.set(Calendar.HOUR, 0)
        selectedCalendar.set(Calendar.MINUTE, 0)
        selectedCalendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.DATE, 1)
        //This will count total days in selected month
        var totalDaysToShow: Int = calendar.getActualMaximum(Calendar.DAY_OF_MONTH)
        //total days extra to be added from previous month
        val toFillInPreviousMonthDays = 1 - calendar.get(Calendar.DAY_OF_WEEK)

        calendar.set(Calendar.DATE, totalDaysToShow)

        //total days extra to be added from next month
        val toFillInNextMonthDays = 7 - calendar.get(Calendar.DAY_OF_WEEK)

        //total days to count max weeks in a month
        totalDaysToShow += abs(toFillInPreviousMonthDays) + toFillInNextMonthDays

        calendar.set(Calendar.DATE, 1)

        if (toFillInPreviousMonthDays != 0) {
            selectedCalendar.add(Calendar.DAY_OF_YEAR, toFillInPreviousMonthDays)
        }

        when (totalDaysToShow.div(7)) {
            4 -> {
                daysRows[4].visibility = View.GONE
                daysRows[5].visibility = View.GONE
            }
            5 -> {
                daysRows[4].visibility = View.VISIBLE
                daysRows[5].visibility = View.GONE
            }
            else -> {
                daysRows[4].visibility = View.VISIBLE
                daysRows[5].visibility = View.VISIBLE
            }
        }

        for (i in 1..totalDaysToShow step 7) {
            fillDaysRow(daysRows[Math.floorDiv(i, 7)], selectedCalendar)
        }
    }

    private fun fillDaysRow(
        dayViewRow: View,
        selectedCalendar: Calendar
    ) {
        val eventsStrip = dayViewRow.findViewById(R.id.events_strip) as LinearLayout

        val minDate: LocalDate = dateToLocalDate(selectedCalendar.time)
        for (i in 1..6) {
            val dayId = resources.getIdentifier("day$i", "id", requireContext().packageName)

            val day = dayViewRow.findViewById(dayId) as TextView
            day.text = selectedCalendar.get(Calendar.DATE).toString()
            selectedCalendar.add(Calendar.DATE, 1)
        }
        val day = dayViewRow.findViewById(R.id.day7) as TextView
        day.text = selectedCalendar.get(Calendar.DATE).toString()
        var maxDate: LocalDate = dateToLocalDate(selectedCalendar.time)
        selectedCalendar.add(Calendar.DATE, 1)

        model.getEvents().forEach { eventItem ->
            val eventStart: LocalDate = dateToLocalDate(dateFormatter.parse(eventItem.start))
            val eventEnd: LocalDate = dateToLocalDate(dateFormatter.parse(eventItem.end))

            if ((minDate <= eventEnd && maxDate >= eventStart)) {
                val eventStrip =
                    layoutInflater.inflate(R.layout.event_strip, null) as CardView
                val eventTitle = eventStrip.findViewById(R.id.txt_eventTitle) as TextView

                eventStrip.setCardBackgroundColor(Color.parseColor(eventItem.color))
                eventTitle.setBackgroundColor(Color.parseColor(eventItem.color))
                eventTitle.text = eventItem.title

                var startMarginDays = 0
                var endMarginDays = 0
                val params = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                )

                if (minDate <= eventStart) {
                    startMarginDays =
                        (widthOfText * minDate.until(eventStart).days) + EXTRA_MARGIN
                }
                if (eventEnd <= maxDate) {
                    endMarginDays =
                        (widthOfText * eventEnd.until(maxDate).days) + EXTRA_MARGIN
                }

                params.setMargins(startMarginDays, 5, endMarginDays, 5)

                eventStrip.setOnLongClickListener {
                    val menu = PopupMenu(context, it, Gravity.END)
                    menu.inflate(R.menu.events_menu)
                    menu.setOnMenuItemClickListener { item ->
                        when (item.itemId) {
                            R.id.event_delete -> {
                                model.removeEvent(eventItem)
                                true
                            }
                            else -> false
                        }
                    }
                    menu.show()
                    true
                }

                eventsStrip.addView(eventStrip, params)
            }
        }
    }

    private fun removeAllEventStripes() {
        daysRows.forEach {
            (it.findViewById(R.id.events_strip) as LinearLayout).removeAllViews()
        }
    }

    private fun dateToLocalDate(date: Date): LocalDate {
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
    }


    companion object {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private const val ARG_SECTION_NUMBER = "section_number"

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        @JvmStatic
        fun newInstance(sectionNumber: Int): CalendarFragment {
            return CalendarFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_SECTION_NUMBER, sectionNumber)
                }
            }
        }

        const val monthYearFormat = "MMM yyyy"
        const val EXTRA_MARGIN = 15
    }
}
