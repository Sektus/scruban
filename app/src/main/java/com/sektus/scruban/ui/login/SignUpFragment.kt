package com.sektus.scruban.ui.login

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.sektus.scruban.R
import com.sektus.scruban.data.model.Status
import com.sektus.scruban.databinding.FragmentSignUpBinding
import com.sektus.scruban.viewmodel.UserViewModel


class SignUpFragment:Fragment(), View.OnClickListener {
    private lateinit var binding: FragmentSignUpBinding
    private val userViewModel: UserViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_sign_up, container, false)
        binding.signUpButton.setOnClickListener(this)
        binding.lifecycleOwner = viewLifecycleOwner

        return binding.root
    }

    override fun onClick(v: View?) {
        when (v?.id){
            binding.signUpButton.id -> signUp()
        }
    }

    private fun signUp(){
        val task = userViewModel.signUp(binding.nick, binding.email, binding.password)
        task.observe(viewLifecycleOwner, Observer {
            Log.d(TAG, "Task triggered")
            if (it.status == Status.SUCCESS){
                Log.d(TAG, "Success")
                findNavController().navigate(
                    SignUpFragmentDirections.showLogin()
                )
            }
        })
        binding.resource = task
    }


    companion object{
        private val TAG = SignUpFragment::class.java.simpleName
    }
}