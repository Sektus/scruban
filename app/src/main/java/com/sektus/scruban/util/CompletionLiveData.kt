package com.sektus.scruban.util

import androidx.lifecycle.LiveData
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.sektus.scruban.data.model.Resource

class CompletionLiveData<T>(private val task: Task<T>, private val msg: String? = null):
    LiveData<Resource<Boolean>>(), OnCompleteListener<T> {

    init {
        value = Resource.loading(msg)
    }

    override fun onActive() {
        super.onActive()
        task.addOnCompleteListener(this)
    }

    override fun onComplete(task: Task<T>) {
        if (task.isSuccessful){
            value = Resource.success(true)
        } else {
            value = Resource.error(task.exception?.localizedMessage)
        }
    }
}