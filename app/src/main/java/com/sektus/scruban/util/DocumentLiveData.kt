package com.sektus.scruban.util

import android.util.Log
import androidx.lifecycle.LiveData
import com.google.firebase.firestore.*
import com.sektus.scruban.data.model.Resource

abstract class DocumentLiveData<T>(private val ref: DocumentReference):
    LiveData<Resource<T>>(), EventListener<DocumentSnapshot> {
    private var registration: ListenerRegistration? = null

    init {
        Log.d(TAG, "Loading document")
        value = Resource.loading(null)
    }

    override fun onEvent(doc: DocumentSnapshot?, error: FirebaseFirestoreException?) {
        if (error != null){
            Log.d(TAG, "Error while loading snapshot ${error.localizedMessage}")
            value = Resource.error(error.localizedMessage)
            return
        }

        value = if (doc == null || doc.data == null){
            Log.d(TAG, "Empty snapshot")
            Resource.success(null)
        } else {
            Log.d(TAG, "New doc: ${doc.data}")
            Resource.success(convert(doc))
        }
    }

    override fun onActive() {
        super.onActive()
        Log.d(TAG, "LiveData is observed")
        startListening()
    }

    override fun onInactive() {
        super.onInactive()
        Log.d(TAG, "LiveData is not observed")
        stopListening()
    }

    fun startListening(){
        if (registration == null) {
            Log.d(TAG, "Start listening for document")
            registration = ref.addSnapshotListener(this)
        }
    }

    fun stopListening(){
        Log.d(TAG, "Stop listening")
        registration?.remove()
        registration = null
    }

    //function used to convert document to model
    abstract fun convert(doc: DocumentSnapshot): T

    companion object {
        private val TAG = DocumentLiveData::class.java.simpleName
    }
}