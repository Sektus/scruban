package com.sektus.scruban.util

import android.util.Log
import androidx.lifecycle.LiveData
import com.google.firebase.firestore.*
import com.sektus.scruban.data.model.Resource

abstract class QueryLiveData<T>(private val query: Query):
    LiveData<Resource<QueryList<T>>>(), EventListener<QuerySnapshot> {
    private val data = QueryList<T>()
    private var registration: ListenerRegistration? = null

    init {
        Log.d(TAG, "Loading resource")
        value = Resource.loading(null, data)
    }

    override fun onEvent(snapshot: QuerySnapshot?, error: FirebaseFirestoreException?) {
        if (error != null) {
            Log.d(TAG, "Error while loading snapshot: ${error.localizedMessage}")
            value = Resource.error(error.localizedMessage)
            return
        }


        for (doc in snapshot!!.documentChanges) {
            Log.d(TAG, "New doc: ${doc.document.data}")
            val element = convert(doc.document)
            when (doc.type) {
                DocumentChange.Type.ADDED -> {
                    data.add(doc.newIndex, element)
                }
                DocumentChange.Type.MODIFIED -> {
                    if (doc.oldIndex != doc.newIndex) {
                        data.remove(doc.oldIndex)
                        value = Resource.success(data)
                        data.add(doc.newIndex, element)
                    } else {
                        data.modify(doc.newIndex, element)
                    }
                }
                DocumentChange.Type.REMOVED -> {
                    data.remove(doc.oldIndex)
                }
            }
            value = Resource.success(data)
        }
    }

    override fun onActive() {
        super.onActive()
        Log.d(TAG, "LiveData is observed")
        startListening()
    }

    override fun onInactive() {
        super.onInactive()
        Log.d(TAG, "LiveData is not observed")
        //stopListening()
    }

    fun startListening(){
        if (registration == null) {
            Log.d(TAG, "Start listening for query")
            registration = query.addSnapshotListener(this)
        }
    }

    fun stopListening(){
        Log.d(TAG, "Stop listening for query")
        registration?.remove()
        registration = null
    }

    abstract fun convert(doc: DocumentSnapshot): T

    companion object {
        private val TAG: String = QueryLiveData::class.java.simpleName
    }
}