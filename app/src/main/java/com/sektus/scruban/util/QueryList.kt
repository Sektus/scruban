package com.sektus.scruban.util

import android.util.Log

class QueryList<T>{
    private val _data = mutableListOf<T>()
    val data: List<T>
        get() = _data

    init {
        Log.d(TAG, "Created")
    }

    private var _changeIndex: Int? = null
    val changeIndex: Int?
        get() = _changeIndex

    private var _changeType: ChangeType? = null
    val changeType: ChangeType?
        get() = _changeType

    fun add(index: Int, element: T) {
        Log.d(TAG, "Added element at index ${index}")
        _data.add(index, element)
        _changeIndex = index
        _changeType = ChangeType.ADDED
    }

    fun modify(index: Int, element: T){
        Log.d(TAG, "Modified element at index ${index}")
        _data[index] = element
        _changeIndex = index
        _changeType = ChangeType.MODIFIED
    }

    fun remove(index: Int){
        Log.d(TAG, "Removed element at index ${index}")
        _data.removeAt(index)
        _changeIndex = index
        _changeType = ChangeType.REMOVED
    }

    enum class ChangeType{
        ADDED, REMOVED, MODIFIED
    }

    companion object {
        private val TAG = QueryList::class.java.simpleName
    }
}
