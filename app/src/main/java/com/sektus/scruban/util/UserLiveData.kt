package com.sektus.scruban.util

import android.util.Log
import androidx.lifecycle.LiveData
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.*
import com.sektus.scruban.data.model.Resource

abstract class UserLiveData<T>(private val users: CollectionReference, private val auth: FirebaseAuth):
    LiveData<Resource<T>>(), FirebaseAuth.AuthStateListener, EventListener<DocumentSnapshot> {
    private var registration: ListenerRegistration? = null
    private var authListenerFlag = false

    init {
        Log.d(TAG, "LiveData created")
    }

    override fun onActive() {
        super.onActive()
        Log.d(TAG, "LiveData is observed")
        startListening()
    }

    override fun onInactive() {
        super.onInactive()
        Log.d(TAG, "LiveData is not observed")
        stopListening()
    }

    fun startListening(){
        if (!authListenerFlag){
            auth.addAuthStateListener(this)
            authListenerFlag = true
        }
    }

    fun stopListening() {
        if (authListenerFlag){
            auth.removeAuthStateListener(this)
            authListenerFlag = false
        }
    }

    override fun onAuthStateChanged(a: FirebaseAuth) {
        val user = auth.currentUser
        Log.d(TAG, "Auth state changed ${user.toString()}")
        if (user == null) {
            value = Resource.success(null)
        } else {
            if (!user.isEmailVerified){
                Log.d(TAG, "email: ${user.email} is not verified")
                value = Resource.error("Email is not verified")
            } else {
                value = Resource.loading("Loading profile...")
                registration = users.document(user.uid).addSnapshotListener(this)
            }
        }
    }

    override fun onEvent(doc: DocumentSnapshot?, error: FirebaseFirestoreException?) {
        if (error != null){
            Log.d(TAG, "Error while loading user document ${error.localizedMessage}")
            value = Resource.error(error.localizedMessage)
        }

        Log.d(TAG, "New doc: ${doc!!.data}")
        value = Resource.success(convert(doc))
    }

    abstract fun convert(doc: DocumentSnapshot): T

    companion object {
        private val TAG = UserLiveData::class.java.simpleName
    }
}