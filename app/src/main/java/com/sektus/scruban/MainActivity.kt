package com.sektus.scruban

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.sektus.scruban.data.model.Status
import com.sektus.scruban.databinding.ActivityMainBinding
import com.sektus.scruban.viewmodel.UserViewModel

class MainActivity : AppCompatActivity(){
    private lateinit var binding: ActivityMainBinding
    private lateinit var navController: NavController
    private val userViewModel: UserViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root

        //check if user is logged in
        userViewModel.user.observe(this, Observer {
            Log.d(TAG, "User triggered $it")
            if (it.status != Status.LOADING && it.data == null){
                startLoginActivity()
            }
        })

        val host: NavHostFragment = supportFragmentManager.findFragmentById(R.id.main_nav_host) as NavHostFragment

        navController = host.navController
        //setupActionBarWithNavController(navController)
        binding.bottomNavigation.setupWithNavController(navController)
        binding.bottomNavigation.setOnNavigationItemReselectedListener {  }

        binding.signOutButton.setOnClickListener {
            signOut()
        }

        setContentView(view)
    }

    private fun signOut(){
        userViewModel.signOut()
        startLoginActivity()
    }

    private fun startLoginActivity(){
        Log.d(TAG, "Started login")
        val i = Intent(baseContext, LoginActivity::class.java)
        startActivity(i)
        finish()
    }

    companion object {
        private val TAG = MainActivity::class.java.simpleName
    }
}
