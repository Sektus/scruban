package com.sektus.scruban.data.model

import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.Exclude

data class User (
    @Exclude @set:Exclude @get:Exclude
    var uid: String = "",
    var nickname: String = "",
    var email: String = "",
    var role: String? = null
) {
    companion object {
        fun fromDoc(doc: DocumentSnapshot): User{
            val user = doc.toObject(User::class.java)!!
            user.uid = doc.id
            return user
        }
    }
}