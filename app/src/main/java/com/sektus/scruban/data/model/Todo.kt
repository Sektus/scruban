package com.sektus.scruban.data.model

import com.google.firebase.Timestamp
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.Exclude
import java.util.*

data class Todo(
    var title: String = "",
    var updateTime: Timestamp = Timestamp(Date()),
    var status: Boolean = false,
    @Exclude @set:Exclude @get:Exclude
    var id: String = ""
) {
    companion object {
        fun fromDoc(doc: DocumentSnapshot): Todo {
            val task = doc.toObject(Todo::class.java)!!
            task.id = doc.id
            return task
        }
    }
}