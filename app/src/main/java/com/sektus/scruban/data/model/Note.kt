package com.sektus.scruban.data.model

import com.google.firebase.Timestamp
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.Exclude
import java.util.*

data class Note (
    var title: String = "",
    var text: String = "",
    var updateTime: Timestamp = Timestamp(Date()),
    @Exclude @set:Exclude @get:Exclude
    var id: String = ""
) {
    fun getFirstLine(): String {
        return this.text.split("\n")[0]
    }

    companion object {
        fun fromDoc(doc: DocumentSnapshot): Note {
            val note = doc.toObject(Note::class.java)!!
            note.id = doc.id
            return note
        }
    }
}