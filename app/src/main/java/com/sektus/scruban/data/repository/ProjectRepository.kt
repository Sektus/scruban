package com.sektus.scruban.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.Timestamp
import com.google.firebase.firestore.*
import com.sektus.scruban.data.model.*
import com.sektus.scruban.repository.FirebaseRepository.getProjectsCollection
import com.sektus.scruban.sevices.Webservice
import com.sektus.scruban.util.CompletionLiveData
import com.sektus.scruban.util.DocumentLiveData
import com.sektus.scruban.util.QueryLiveData
import org.w3c.dom.Document
import java.util.*
import kotlin.collections.HashMap

class ProjectRepository {
    private val db = Webservice.getInstance()
    private val cachedUser = HashMap<String, DocumentLiveData<User>>()

    fun addUser(project: Project, nick: String) {
        db.getUsers().whereEqualTo("nickname", nick).get().addOnCompleteListener {
            val result = it.result
            if (result != null) {
                if (!result.isEmpty){
                    val user = User.fromDoc(result.documents[0])
                    db.addUserProject(user.uid, project)
                    user.role = "Developer"
                    db.addProjectUser(project.id, user)
                }
            }
        }
    }

    fun loadProjects(uid: String): QueryLiveData<Project> {
        val query = db.getUserProjects(uid).orderBy("updateTime")

        return object: QueryLiveData<Project>(query) {
            override fun convert(doc: DocumentSnapshot): Project = Project.fromDoc(doc)
        }
    }

    fun loadProject(pid: String) : DocumentLiveData<Project> {
        return object: DocumentLiveData<Project>(db.getProject(pid)) {
            override fun convert(doc: DocumentSnapshot): Project = Project.fromDoc(doc)
        }
    }

    fun loadUsers(pid: String): QueryLiveData<User> {
        val query = db.getProjectUsers(pid).orderBy("role")
        return object : QueryLiveData<User>(query) {
            override fun convert(doc: DocumentSnapshot): User = User.fromDoc(doc)
        }
    }
    fun loadHistory(pid: String): QueryLiveData<History> {
        val query = db.getProjectHistory(pid)
        return object : QueryLiveData<History>(query) {
            override fun convert(doc: DocumentSnapshot): History = History.fromDoc(doc)
        }
    }

    fun loadUser(pid: String, uid: String): DocumentLiveData<User> {
        val cached = cachedUser[pid + uid]
        if (cached != null) {
            return cached
        }

        val data = object: DocumentLiveData<User>(db.getProjectUser(pid, uid)){
            override fun convert(doc: DocumentSnapshot): User = User.fromDoc(doc)
        }

        cachedUser[pid + uid] = data
        return data
    }

    fun createProject(user: User, title: String): LiveData<Resource<Boolean>> {
        val project = Project(title)
        val maintainer = user.copy()
        maintainer.role = "Maintainer"

        val data = MutableLiveData<Resource<Boolean>>(Resource.loading("Creating project..."))
        db.addProject(project).addOnCompleteListener { createProject ->
            if (createProject.isSuccessful) {
                project.id = createProject.result!!.id
                db.addUserProject(user.uid, project).addOnCompleteListener { addProjectToUser ->
                    if (addProjectToUser.isSuccessful) {
                       db.addProjectUser(project.id, maintainer).addOnCompleteListener {addUserToProject ->
                           if (addUserToProject.isSuccessful) {
                               data.value = Resource.success(true)
                           } else {
                               data.value = Resource.error(addUserToProject.exception!!.localizedMessage)
                           }
                       }
                    } else {
                        data.value = Resource.error(addProjectToUser.exception!!.localizedMessage)
                    }
                }
            } else {
                data.value = Resource.error(createProject.exception!!.localizedMessage)
            }
        }

        return data
    }

    companion object {
        private val TAG = ProjectRepository::class.java.simpleName
        private var INSTANCE: ProjectRepository? = null

        fun getInstance(): ProjectRepository =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildInstance().also { INSTANCE = it }
            }

        private fun buildInstance() = ProjectRepository()
    }
}