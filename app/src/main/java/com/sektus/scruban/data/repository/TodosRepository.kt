package com.sektus.scruban.data.repository

import com.google.firebase.Timestamp
import com.google.firebase.firestore.*
import com.sektus.scruban.data.model.Todo
import com.sektus.scruban.sevices.Webservice
import com.sektus.scruban.util.CompletionLiveData
import com.sektus.scruban.util.QueryLiveData
import java.util.*

class TodosRepository {
    private val db = Webservice.getInstance()

    fun loadTodos(uid: String): QueryLiveData<Todo> {
        val query = db.getTodos(uid).orderBy("status")
                                          .orderBy("updateTime", Query.Direction.DESCENDING)
        return object: QueryLiveData<Todo>(query) {
            override fun convert(doc: DocumentSnapshot): Todo = Todo.fromDoc(doc)
        }
    }

    fun addTodo(uid: String, title: String): CompletionLiveData<DocumentReference> {
        val todo = Todo(title)
        return CompletionLiveData(db.addTodo(uid, todo), "Creating task...")
    }

    fun updateTodo(uid: String, todo: Todo): CompletionLiveData<Void> {
        todo.updateTime = Timestamp(Date())
        return CompletionLiveData(db.setTodo(uid, todo), null)
    }

    fun deleteTodo(uid: String, todo: Todo): CompletionLiveData<Void> {
        return CompletionLiveData(db.deleteTodo(uid, todo), null)
    }

    companion object {
        private val TAG = TodosRepository::class.java.simpleName
        private var INSTANCE: TodosRepository? = null

        fun getInstance(): TodosRepository =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildInstance().also { INSTANCE = it }
            }

        private fun buildInstance() = TodosRepository()
    }
}