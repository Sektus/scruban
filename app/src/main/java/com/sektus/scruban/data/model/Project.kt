package com.sektus.scruban.data.model

import com.google.firebase.Timestamp
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.Exclude
import java.util.*

data class Project (
    var title: String = "",
    var updateTime: Timestamp = Timestamp(Date()),
    @Exclude @set:Exclude @get: Exclude
    var id: String = ""
) {
    companion object {
        fun fromDoc(doc: DocumentSnapshot): Project {
            val project = doc.toObject(Project::class.java)!!
            project.id = doc.id
            return project
        }
    }
}
