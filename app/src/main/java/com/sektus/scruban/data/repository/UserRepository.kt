package com.sektus.scruban.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.Query
import com.sektus.scruban.data.model.Resource
import com.sektus.scruban.data.model.User
import com.sektus.scruban.sevices.Webservice
import com.sektus.scruban.util.CompletionLiveData
import com.sektus.scruban.util.UserLiveData

class UserRepository {
    private val db = Webservice.getInstance()
    private val auth = FirebaseAuth.getInstance()
    private var cachedUser: UserLiveData<User>? = null

    fun loadUser(): UserLiveData<User> {
        val cached = cachedUser

        return if (cached == null){
            val data = object: UserLiveData<User>(db.getUsers(), auth) {
                override fun convert(doc: DocumentSnapshot): User = User.fromDoc(doc)
            }
            cachedUser = data
            data
        } else {
            cached
        }
    }

    fun findNickname(nick: String) : Query {
        return db.getUsers().whereEqualTo("nickname", nick)
    }

    fun signUp(nick: String?, email: String?, password: String?): LiveData<Resource<Boolean>>{
        val data = MutableLiveData<Resource<Boolean>>(Resource.loading("Checking credentials..."))

        val error = validateCredentials(nick, email, password)
        if (error != ""){
            data.value = Resource.error(error)
            return data
        }

        /* stop listening for auth state before create new user
           because it triggers email verification dialog
         */
        cachedUser?.stopListening()
        data.value = Resource.loading("Checking nickname...")

        findNickname(nick!!).get()
            .addOnCompleteListener {nicknameExists ->
                if (!nicknameExists.isSuccessful){
                    data.value = Resource.error(nicknameExists.exception!!.localizedMessage)
                    cachedUser?.startListening()
                } else {
                    if (nicknameExists.result!!.isEmpty) {
                        data.value = Resource.loading("Creating profile...")
                        auth.createUserWithEmailAndPassword(email!!, password!!)
                            .addOnCompleteListener {authResult ->
                                if (!authResult.isSuccessful){
                                    data.value = Resource.error(authResult.exception!!.localizedMessage)
                                    cachedUser?.startListening()
                                } else {
                                    //successful task, so new user is signed in, no null check required
                                    val uid = authResult.result!!.user!!.uid
                                    val user = User(uid, nick, email)

                                    db.setUser(uid, user) .addOnCompleteListener { task ->
                                        if (task.isSuccessful) {
                                            data.value = Resource.loading("Sending verification email")
                                            auth.currentUser!!.sendEmailVerification()
                                                .addOnCompleteListener {
                                                    if (it.isSuccessful) {
                                                        data.value = Resource.success(true)
                                                    } else {
                                                        data.value =
                                                            Resource.error("Error while sending verification email")
                                                    }
                                                    auth.signOut()
                                                    cachedUser?.startListening()
                                                }

                                        } else {
                                            data.value =
                                                Resource.error(task.exception?.localizedMessage)
                                            auth.currentUser?.delete()
                                            auth.signOut()
                                            cachedUser?.startListening()
                                        }
                                    }
                                }
                            }
                    } else {
                        data.value = Resource.error("Nickname is already in use")
                        cachedUser?.startListening()
                    }
                }
            }
        return data
    }

    private fun validateCredentials(nick: String?, email: String?, password: String?): String{
        var error = validateNickname(nick)
        if (error == ""){
            error = validateEmail(email)
            if (error == ""){
                error = validatePassword(nick!!, email!!, password)
            }
        }
        return error
    }

    private fun validateNickname(nick: String?): String {
        return when {
            nick == null -> "Nickname is required"
            nick.isEmpty() -> "Nickname is required"
            nick.length < 3 -> "Nickname is too short"
            nick.length > 15 -> "Nickname is too long"
            !nick.matches("[A-Za-z0-9_-]{3,15}".toRegex()) -> "Only a–Z0–9_- characters are allowed"
            else -> ""
        }
    }

    private fun validateEmail(email: String?): String {
        return when {
            email == null -> "Email is required"
            email.isEmpty() -> "Email is required"
            !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches() -> "Wrong email format"
            else -> ""
        }
    }

    private fun validatePassword(nick: String, email: String, pass: String?): String {
        return when {
            pass == null -> "Password is required"
            pass.isEmpty() -> "Password is required"
            pass.length < 8 -> "Password is too short"
            pass.length > 24 -> "Password is too long"
            !pass.matches(".*\\d.*".toRegex()) -> "Password should contain at least one digit"
            !pass.matches(".*[a-z].*".toRegex()) -> "Password should contain at least one lowercase letter"
            !pass.matches(".*[!@#$%^&*+=?-_].*".toRegex()) -> "Password should contain at least one special character"
            pass.contains(nick) -> "Password contains nickname"
            email.contains(pass) -> "Email contains password"
            else -> ""
        }
    }

    fun signIn(email: String?, password: String?): LiveData<Resource<Boolean>> {
        if (email == null){
            return MutableLiveData(Resource.error("Email is required"))
        } else if (password == null){
            return MutableLiveData(Resource.error("Password is required"))
        }
        val task = auth.signInWithEmailAndPassword(email, password)
        return CompletionLiveData(task, "Signing in...")
    }

    fun signOut(){
        //cachedUser = null
        auth.signOut()
    }

    fun sendEmailVerification(): LiveData<Resource<Boolean>>{
        val task = auth.currentUser!!.sendEmailVerification()
        return CompletionLiveData(task, "Sending verification email...")
    }

    companion object {
        private val TAG = UserRepository::class.java.simpleName
        private var INSTANCE: UserRepository? = null

        fun getInstance(): UserRepository =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildInstance().also { INSTANCE = it }
            }

        private fun buildInstance() = UserRepository()
    }
}