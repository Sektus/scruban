package com.sektus.scruban.data.repository

import com.google.firebase.Timestamp
import com.google.firebase.firestore.*
import com.sektus.scruban.data.model.Note
import com.sektus.scruban.sevices.Webservice
import com.sektus.scruban.util.CompletionLiveData
import com.sektus.scruban.util.QueryLiveData
import java.util.*

class NotesRepository {
    private val db = Webservice.getInstance()

    fun loadNotes(uid: String): QueryLiveData<Note> {
        val query = db.getNotes(uid).orderBy("updateTime")
        return object : QueryLiveData<Note>(query) {
            override fun convert(doc: DocumentSnapshot): Note = Note.fromDoc(doc)
        }
    }

    fun createNote(uid: String, title: String, text: String): CompletionLiveData<DocumentReference> {
        val note = Note(title, text)
        return CompletionLiveData(db.addNote(uid, note), "Adding note...")
    }

    fun saveNote(uid: String, note: Note): CompletionLiveData<Void> {
        note.updateTime = Timestamp(Date())
        return CompletionLiveData(db.setNote(uid, note))
    }

    fun deleteNote(uid: String, note: Note): CompletionLiveData<Void> {
        return CompletionLiveData(db.deleteNote(uid, note), "Removing note...")
    }

    companion object {
        private val TAG = NotesRepository::class.java.simpleName
        private var INSTANCE: NotesRepository? = null

        fun getInstance(): NotesRepository =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildInstance().also { INSTANCE = it }
            }

        private fun buildInstance() = NotesRepository()
    }
}