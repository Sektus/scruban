package com.sektus.scruban.data.model
import com.google.firebase.firestore.Exclude

data class Event(
    var title: String = "My Event",
    var start: String? = null,
    var end: String? = null,
    var color: String = "#5b45ee",
    @Exclude @set:Exclude @get:Exclude
    var id: String = ""
)