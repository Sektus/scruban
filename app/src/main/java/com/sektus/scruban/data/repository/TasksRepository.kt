package com.sektus.scruban.data.repository

import com.google.firebase.Timestamp
import com.google.firebase.firestore.*
import com.sektus.scruban.data.model.History
import com.sektus.scruban.data.model.Project
import com.sektus.scruban.data.model.TaskItem
import com.sektus.scruban.data.model.User
import com.sektus.scruban.sevices.Webservice
import com.sektus.scruban.util.CompletionLiveData
import com.sektus.scruban.util.DocumentLiveData
import com.sektus.scruban.util.QueryLiveData
import java.util.*
import kotlin.collections.HashMap

class TasksRepository {
    private val db = Webservice.getInstance()
    private val cachedActiveTask =  HashMap<String, DocumentLiveData<TaskItem>>()
    private val cachedDoingTasks = HashMap<String, QueryLiveData<TaskItem>>()

    fun addHistory(pid: String, history: History): CompletionLiveData<DocumentReference> {
        return CompletionLiveData(db.addProjectHistory(pid, history), "")
    }

    fun getActiveTask(uid: String): DocumentLiveData<TaskItem> {
        val cached = cachedActiveTask[uid]
        if (cached != null) {
            return cached
        }

        val data = object: DocumentLiveData<TaskItem>(db.getActiveTask(uid)){
            override fun convert(doc: DocumentSnapshot): TaskItem = TaskItem.fromDoc(doc)
        }

        cachedActiveTask[uid] = data
        return data
    }

    fun onTaskClick(user: User, project: Project, task: TaskItem) {
        if (!task.started || task.user == user.nickname){
            db.runTransaction {transaction ->
                //check if user have active task
                val snapshot = transaction.get(db.getActiveTask(user.uid))
                if (snapshot.exists()){
                    val task = TaskItem.fromDoc(snapshot)
                    val ref = task.projectRef!!
                    task.started = false
                    task.projectRef = null
                    task.projectTitle = null
                    task.user = null

                    val time = Timestamp(Date()).seconds - task.updateTime.seconds
                    task.time += time

                    transaction.delete(snapshot.reference)
                    val histrory = History(time, user.nickname, task.id, task.title)
                    transaction.set(db.getProjectHistory(ref.parent.parent!!.id).document(), histrory)
                    transaction.set(ref, task)
                }
                if (!task.started) {
                    //set new task
                    task.user = user.nickname
                    task.updateTime = Timestamp(Date())
                    task.started = true
                    if (task.status == "todo") task.status = "doing"
                    transaction.set(db.getProjectTask(project.id, task.id), task)
                    task.projectRef = db.getProjectTask(project.id, task.id)
                    task.projectTitle = project.title
                    transaction.set(db.getActiveTask(user.uid), task)
                }

                null
            }
        }
    }

    fun taskQuery(pid: String, status: String): Query {
        return db.getProjectTasks(pid).whereEqualTo("status", status)
    }

    fun loadAllTasks(pid: String): QueryLiveData<TaskItem> {
        return TaskLiveData(db.getProjectTasks(pid))
    }

    fun loadTodoTasks(pid: String): QueryLiveData<TaskItem> {
        return TaskLiveData(taskQuery(pid, "todo"))
    }

    fun loadDoingTasks(pid: String): QueryLiveData<TaskItem> {
        val cached = cachedDoingTasks[pid]
        if (cached != null) {
            return cached
        }

        val data = TaskLiveData(taskQuery(pid, "doing"))

        cachedDoingTasks[pid] = data
        return data
    }

    fun loadDoneTasks(pid: String): QueryLiveData<TaskItem> {
        return TaskLiveData(taskQuery(pid, "done"))
    }

    fun addTask(pid: String, title: String): CompletionLiveData<DocumentReference> {
        val task = TaskItem(title)
        return CompletionLiveData(db.addProjectTask(pid, task), "Creating task...")
    }

    fun updateTask(pid: String, task: TaskItem): CompletionLiveData<Void> {
        task.updateTime = Timestamp(Date())
        return CompletionLiveData(db.setProjectTask(pid, task), "Modify task...")
    }

    fun removeTask(pid: String, task: TaskItem): CompletionLiveData<Void> {
        return CompletionLiveData(db.deleteProjectTask(pid, task.id), "Removing task...")
    }

    private class TaskLiveData(query: Query): QueryLiveData<TaskItem>(query) {
        override fun convert(doc: DocumentSnapshot): TaskItem = TaskItem.fromDoc(doc)
    }

    companion object {
        private val TAG = TasksRepository::class.java.simpleName
        private var INSTANCE: TasksRepository? = null

        fun getInstance(): TasksRepository =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildInstance().also { INSTANCE = it }
            }

        private fun buildInstance() = TasksRepository()
    }
}