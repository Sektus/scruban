package com.sektus.scruban.data.model

import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.Exclude

data class History(
    var time: Long = 0,
    var user: String = "",
    var tid: String = "", //task id
    var title: String = "", //task title
    @Exclude @set:Exclude @get:Exclude
    var id: String = ""
) {
    companion object {
        fun fromDoc(doc: DocumentSnapshot): History {
            val history = doc.toObject(History::class.java)!!
            history.id = doc.id
            return history
        }
    }
}