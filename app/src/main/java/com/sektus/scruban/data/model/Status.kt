package com.sektus.scruban.data.model

enum class Status {
    SUCCESS, ERROR, LOADING
}