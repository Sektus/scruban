package com.sektus.scruban.data.model

data class Resource<out T>(val status: Status, val data: T?, val message: String?)
{
    companion object{
        /* success status */
        fun <T> success(data: T?): Resource<T> {
            return Resource(Status.SUCCESS, data, null)
        }

        /* error status */
        fun <T> error(msg: String?, data: T? = null): Resource<T> {
            return Resource(Status.ERROR, data, msg)
        }

        /* loading status */
        fun <T> loading(msg: String? = null, data: T? = null): Resource<T> {
            return Resource(Status.LOADING, data, msg)
        }
    }
}