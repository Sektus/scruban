package com.sektus.scruban.data.model

import androidx.lifecycle.MutableLiveData
import com.google.firebase.Timestamp
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.Exclude
import java.util.*

data class TaskItem (
    var title: String = "",
    var status: String = "todo", // states: "todo", "doing", "done"
    var time: Long = 0,
    var user: String? = null,
    var started: Boolean = false,
    var updateTime: Timestamp = Timestamp(Date()),
    var projectRef: DocumentReference? = null,
    var projectTitle: String? = null,
    @Exclude @set:Exclude @get:Exclude
    var id: String = "",
    @Exclude @set:Exclude @get:Exclude
    var time_text: MutableLiveData<String> = MutableLiveData("00:00:00")
) {
    companion object {
        fun timeToString(time: Long): String {
            val seconds = time % 60
            val minutes = (time / 60) % 60
            val hours = (time / 60 / 60) % 24
            return String.format("%02d:%02d:%02d", hours, minutes, seconds)
        }

        fun fromDoc(doc: DocumentSnapshot): TaskItem {
            val task = doc.toObject(TaskItem::class.java)!!
            task.id = doc.id
            task.time_text = MutableLiveData(timeToString(task.time))
            return task
        }
    }
}